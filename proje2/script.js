"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var ListingApp = function (_React$Component) {
    _inherits(ListingApp, _React$Component);

    function ListingApp() {
        _classCallCheck(this, ListingApp);

        var _this = _possibleConstructorReturn(this, (ListingApp.__proto__ || Object.getPrototypeOf(ListingApp)).call(this));

        _this.state = {
            cities: ["İstanbul", "Ankara", "İzmir"]
        };

        _this.onSubmit = _this.onSubmit.bind(_this);
        _this.onRemove = _this.onRemove.bind(_this);
        return _this;
    }

    _createClass(ListingApp, [{
        key: "onSubmit",
        value: function onSubmit(e) {

            e.preventDefault();

            var cityName = e.target.elements.city.value;

            e.target.elements.city.value = '';

            this.setState(function (prevState) {
                return {
                    cities: prevState.cities.concat([cityName])
                };
            });
        }
    }, {
        key: "onRemove",
        value: function onRemove(index) {

            this.setState(function (prevState) {

                var cities = prevState.cities;
                cities.splice(index, 1);

                // key ve value ismi aynı ise
                return {
                    cities: cities
                };
            });
        }
    }, {
        key: "render",
        value: function render() {
            var _this2 = this;

            return React.createElement(
                "div",
                null,
                React.createElement(
                    "h1",
                    null,
                    "\u015Eehirler"
                ),
                this.state.cities.map(function (item, index) {
                    return React.createElement(
                        "li",
                        { key: index },
                        item,
                        React.createElement(
                            "button",
                            { onClick: function onClick(e) {
                                    _this2.onRemove(index);
                                } },
                            "S\u0130L"
                        )
                    );
                }),
                React.createElement(
                    "div",
                    null,
                    React.createElement(
                        "form",
                        { onSubmit: this.onSubmit },
                        React.createElement("input", { name: "city", placeholder: "Yeni \u015Fehir ismi.." }),
                        React.createElement("br", null),
                        React.createElement(
                            "button",
                            null,
                            "Ekle"
                        )
                    )
                )
            );
        }
    }]);

    return ListingApp;
}(React.Component);

var root = document.getElementById('app');
ReactDOM.render(React.createElement(ListingApp, null), root);
