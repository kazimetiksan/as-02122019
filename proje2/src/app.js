class ListingApp extends React.Component {

    constructor () {
        super()

        this.state = {
            cities: [
                "İstanbul", 
                "Ankara", 
                "İzmir"
            ]
        }

        this.onSubmit = this.onSubmit.bind(this)
        this.onRemove = this.onRemove.bind(this)
    }

    onSubmit (e) {

        e.preventDefault()

        const cityName = e.target.elements.city.value

        e.target.elements.city.value = ''

        this.setState((prevState) => {
            return {
                cities: prevState.cities.concat([cityName])
            }
        })
    }

    onRemove (index) {

        this.setState((prevState) => {

            let cities = prevState.cities
            cities.splice(index, 1)

            // key ve value ismi aynı ise
            return {
                cities
            }
        })
    }

    render () {

        return (
            <div>
                <h1>Şehirler</h1>
                {
                    this.state.cities.map((item,index) => {
                        return (
                            <li key={index}>{item}
                                <button onClick={(e) => {
                                    this.onRemove(index)
                                }}>SİL</button>
                            </li>
                        )
                    })
                }
                <div>
                    <form onSubmit={this.onSubmit}>
                        <input name="city" placeholder="Yeni şehir ismi.." /><br />
                        <button>Ekle</button>
                    </form>
                </div>
            </div>
        )
    }
}


var root = document.getElementById('app')
ReactDOM.render(<ListingApp />, root)