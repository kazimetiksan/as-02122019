import React from 'react'
import {Route,Redirect} from 'react-router-dom'

const isLoggedIn = () => {
    const token = sessionStorage.getItem('xauth')
    return token != undefined
}

// const person = {
//     firstName: "Kazım",
//     lastName: "Etiksan"
// }

// const {firstName:onadi,lastName,age=39} = person

export const Private = ({component:RoutedComponent, ...routeProps}) => {
    return (
        isLoggedIn() ?
            <Route render={(props) => {
                return <RoutedComponent {...props} />
            }} 
            {...routeProps}
            />
        :
            <Redirect to="/login" />
    )
}