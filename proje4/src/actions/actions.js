import {ADD,REMOVE,SET_ALL,EDIT} from '../reducers/identifiers'
import axios from 'axios'

export const setAllAction = (data) => {
    return {
        type: SET_ALL,
        data
    }
}

export const asyncAddAction = ({
    firstName='',
    lastName='',
    classroom='arılar'
}, callback) => {
    return (dispatch) => {

        const newStudent = {
            firstName,
            lastName,
            classroom
        }

        axios.post('/api/student', newStudent)
        .then((response) => {
            dispatch(addAction(response.data.data[0]))
            callback()
        })
        .catch((err) => {
        })
    }
}

export const addAction = (data) => {
    return {
        type: ADD,
        data
    }
}

export const asyncEditAction = (editingId,data) => {
    return (dispatch) => {

        axios.patch(`https://std02.herokuapp.com/api/student/${editingId}`, data)
        .then((response) => {

            const editedStudent = response.data.data[0]
            dispatch(editAction(editedStudent._id,{
                firstName: editedStudent.firstName,
                lastName: editedStudent.lastName,
                classroom: editedStudent.classroom
            }))
        })
        .catch((err) => {
        })
    }
}

export const editAction = (editingId,data) => {
    return {
        type: EDIT,
        editingId,
        data
    }
}

export const asyncRemoveAction = (_id) => {
    return (dispatch) => {
        axios.delete(`https://std02.herokuapp.com/api/student/${_id}`)
        .then(() => {
            dispatch(removeAction(_id))
        })
        .catch((err) => {
        })
    }
}

export const removeAction = (_id) => {
    return {
        type: REMOVE,
        _id
    }
}