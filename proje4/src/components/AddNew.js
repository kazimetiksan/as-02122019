import React from 'react'
import NewForm from './NewForm'
import {connect} from 'react-redux'
import {asyncAddAction} from '../actions/actions'

class AddNew extends React.Component {

    constructor () {
        super()

        this.addStudent = this.addStudent.bind(this)
    }

    componentDidMount () {
        console.log('addnew.js did mount', this.props)
    }

    addStudent (newStudent) {

        const {history,addNewStudent} = this.props

        // this.props.dispatch(addAction(newStudent))
        addNewStudent(newStudent, () => {
            history.push('/')
        })
    }

    render () {

        return (
            <div>
                <h1>Yeni Ekle</h1>
                <NewForm addStudent={this.addStudent} />
            </div>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        addNewStudent: (data,callback) => {dispatch(asyncAddAction(data,callback))}
    }
}

export default connect(null,mapDispatchToProps)(AddNew)