import React from 'react'
import NewForm from './NewForm'
import {connect} from 'react-redux'
import {asyncEditAction} from '../actions/actions'

class Edit extends React.Component {

    constructor () {
        super()

        this.updateStudent = this.updateStudent.bind(this)
    }

    updateStudent (_id, data) {
        console.log(_id, data)

        const {saveEditedStudent,history} = this.props

        // this.props.dispatch(editAction(_id, data))

        saveEditedStudent(_id, data)
        history.push('/')
    }

    render () {

        const userId = this.props.match.params._id
        const {students} = this.props

        const student = students.find((item) => {
            return item._id == userId
        })

        return (
            <div>
                <h1>Edit</h1>
                <NewForm 
                    editingStudent={student} 
                    updateStudent={this.updateStudent}
                />
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        students: state
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        saveEditedStudent: (editingId,data) => {dispatch(asyncEditAction(editingId,data))}
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(Edit)