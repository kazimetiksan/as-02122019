import React from 'react'
import uuid from 'uuid'

export default (props) => {

    const {addStudent,editingStudent,updateStudent} = props

    const onSubmit = (e) => {

        e.preventDefault()

        const firstName = e.target.elements.firstName.value
        const lastName = e.target.elements.lastName.value
        const classroom = e.target.elements.classroom.value

        const data = {
            firstName,
            lastName,
            classroom
        }

        if (editingStudent == undefined) {
    
            addStudent(data)
        } else {

            updateStudent(editingStudent._id, data)
        }
    }

    return (
        <form onSubmit={onSubmit}>
            <input name="firstName" placeholder="Ad" defaultValue={editingStudent == undefined ? '' : editingStudent.firstName} /><br />
            <input name="lastName" placeholder="Soyad" defaultValue={editingStudent == undefined ? '' : editingStudent.lastName} /><br />
            <input name="classroom" placeholder="Sınıf"  defaultValue={editingStudent == undefined ? '' : editingStudent.classroom} /><br />
            <button>{editingStudent == undefined ? 'EKLE' : 'GÜNCELLE'}</button>
        </form>
    )
}