import React from 'react'

export default (props) => {

    const {student,editStudent,removeStudent} = props

    return (
        <tr>
            <td>{student.firstName}</td>
            <td>{student.lastName}</td>
            <td>{student.classroom}</td>
            <td>
                <button 
                    onClick={() => {
                        editStudent(student._id)
                    }}>
                    EDIT
                </button>
            </td>
            <td>
                <button 
                    onClick={() => {
                        removeStudent(student._id)
                    }}>
                    SİL
                </button>
            </td>
        </tr>
    )
}