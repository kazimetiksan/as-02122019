import React, {useState} from 'react'
import axios from 'axios'

const Login = (props) => {

    console.log(props)

    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')

    const onSubmit = (e) => {
        e.preventDefault()

        axios.post('/api/users/login',{
            email,
            password
        })
        .then((response) => {
            console.log(response)

            const token = response.headers.xauth
            sessionStorage.setItem('xauth',token)

            const userStr = JSON.stringify({
                email,
                password
            })

            sessionStorage.setItem('userInfo',userStr)

            props.history.push('/')

            // const userInfo = JSON.parse(sessionStorage.getItem('userInfo'))

            // localStorage.setItem('firstName', 'Kazım')
            // localStorage.getItem('firstName')
        })
    }

    return (
        <div>
            <h1>Kullanıcı Girişi</h1>
            <form onSubmit={onSubmit}>
                <input name="email" placeholder="Email" value={email} onChange={(e) => {
                    setEmail(e.target.value)
                }} /><br />
                <input name="password" placeholder="Şifre" value={password} onChange={(e) => {
                    setPassword(e.target.value)
                }} /><br />
                <button>Giriş Yap</button>
            </form>
        </div>
    )
}

export default Login