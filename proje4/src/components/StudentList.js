import React from 'react'
import StudentRow from './StudentRow'
import {connect} from 'react-redux'
import {asyncRemoveAction} from '../actions/actions'

class StudentList extends React.Component {

    componentDidMount () {
        console.log('student list did mount', this.props)
    }

    constructor () {
        super()

        this.editStudent = this.editStudent.bind(this)
        this.removeStudent = this.removeStudent.bind(this)
    }

    editStudent (editingId) {

        const {history} = this.props
        history.push(`/edit/${editingId}`)
    }

    removeStudent (removingId) {

        this.props.dispatch(asyncRemoveAction(removingId))
    }

    render () {

        const {students} = this.props

        return (
            <div>
            {
                students.length > 0 ?
                (
                    <table>
                        <tbody>
                            <tr>
                                <td>Ad</td>
                                <td>Soyad</td>
                                <td>Sınıf</td>
                            </tr>
                            {
                                students.map((std,index) => {
                                    return (
                                        <StudentRow 
                                            key={index} 
                                            student={std}
                                            editStudent={this.editStudent}
                                            removeStudent={this.removeStudent}
                                        />
                                    )
                                })
                            }
                        </tbody>
                    </table>
                )
                : 'Loading..'
            }
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        students: state
    }
}

export default connect(mapStateToProps)(StudentList)