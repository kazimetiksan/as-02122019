import React from 'react'
import StudentList from './StudentList'
import {connect} from 'react-redux'

class Dashboard extends React.Component {

    render () {

        return (
            <div>
                <h1>Dashboard</h1>
                <StudentList history={this.props.history} />
            </div>
        )
    }
}

export default connect()(Dashboard)