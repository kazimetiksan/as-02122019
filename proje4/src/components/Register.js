import React, {useState,useEffect} from 'react'
import validator from 'validator'
import axios from 'axios'

const Register = () => {

    const [name,setName] = useState('')
    const [email,setEmail] = useState('')
    const [password,setPassword] = useState('')

    useEffect(() => {
        console.log('component did mount')
    },[])

    useEffect(() => {
        console.log('name güncellendi')
    },[name])

    const onSubmit = (e) => {

        e.preventDefault()

        if (!validator.isEmail(email)) {
            alert('Hatalı email!')
            return
        }

        const newUser = {
            name,
            email,
            password
        }

        axios.post('/api/users', newUser)
        .then((response) => {
            console.log(response)
            this.props.history.push('/')
        })
    }

    return (
        <div>
            <h1>Yeni Kullanıcı</h1>
            <form onSubmit={onSubmit}>
                <input name="name" placeholder="Ad Soyad" value={name} onChange={(e) => {
                    setName(e.target.value)
                }} /><br />
                <input name="email" placeholder="Email" value={email} onChange={(e) => {
                    setEmail(e.target.value)
                }} /><br />
                <input name="password" placeholder="Şifre" value={password} onChange={(e) => {
                    setPassword(e.target.value)
                }} /><br />
                <button>Kaydet</button>
            </form>
        </div>
    )

}

export default Register