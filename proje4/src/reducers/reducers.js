import {ADD,REMOVE,SET_ALL,EDIT} from './identifiers'

export const studentReducer = (state=[],action) => {

    switch (action.type) {

        case SET_ALL:
            return action.data

        case ADD:
            return [...state, action.data]

        case EDIT:
            return state.map((item) => {

                if (item._id == action.editingId) {
                    return {
                        ...item,
                        ...action.data
                    }
                }

                return item
            })

        case REMOVE:
            return state.filter((item) => {
                return item._id != action._id
            })

        default: return state
    }
}