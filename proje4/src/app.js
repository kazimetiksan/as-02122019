import React from 'react'
import ReactDOM from 'react-dom'
import Dashboard from './components/Dashboard'
import AddNew from './components/AddNew'
import Edit from './components/Edit'
import Login from './components/Login'
import Register from './components/Register'
import Header from './components/Header'
import {Route, BrowserRouter, Switch} from 'react-router-dom'
import {Provider} from 'react-redux'
import configureStore from './store/configureStore'
import axios from 'axios'
import {setAllAction} from './actions/actions'
import {Private} from './routes/routes'

const store = configureStore()

class App extends React.Component {

    componentDidMount () {

        const url = '/api/student'

        axios.get(url)
        .then((response) => {
            console.log(response.data.list)
            store.dispatch(setAllAction(response.data.list))
        })
        .catch((err) => {

        })
    }

    render () {
        return (
            <Provider store={store}>
                <BrowserRouter>
                    <Header />
                    <Switch>
                        <Route path="/" component={Dashboard} exact={true} />
                        <Private path="/add" component={AddNew} />
                        <Route path="/edit/:_id" component={Edit} />
                        <Route path="/register" component={Register} />
                        <Route path="/login" component={Login} />
                    </Switch>
                </BrowserRouter>
            </Provider>
        )
    }
}

var root = document.getElementById('app')
ReactDOM.render(<App />, root)