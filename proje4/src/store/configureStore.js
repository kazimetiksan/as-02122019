import {studentReducer} from '../reducers/reducers'
import {createStore,applyMiddleware} from 'redux'
import thunk from 'redux-thunk'

export default () => {
    return createStore(studentReducer,applyMiddleware(thunk))
}