console.log('redux js loaded')

import {createStore} from 'redux'

const demoState = {
    count: 1
}

const store = createStore((state=demoState,action) => {
    switch (action.type) {

        case 'RESET': 
            return {
                count: action.resetTo
            }

        case 'INCREMENT': 
            return {
                count: state.count+action.incrementBy
            }

        case 'DECREMENT': 
            return {
                count: state.count-action.decrementBy
            }

        default: return state
    }
})

console.log(store.getState())

const incrementAction = (incrementBy=1) => {
    return {
        type: 'INCREMENT',
        incrementBy
    }
}

store.dispatch(incrementAction(100))

console.log(store.getState())

const decrementAction = (decrementBy=1) => {
    return {
        type: 'DECREMENT',
        decrementBy
    }
}

store.dispatch(decrementAction(50))

console.log(store.getState())

const resetAction = (resetTo=1) => {
    return {
        type: 'RESET',
        resetTo
    }
}

store.dispatch(resetAction(10))

console.log(store.getState())