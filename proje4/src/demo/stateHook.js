import React, {useState,useEffect} from 'react'
import ReactDOM from 'react-dom'

const SomeComp = () => {

    useEffect(() => {
        console.log('state güncellendi')
    })

    const [count, setCount] = useState(0)

    return (
        <div>
            <h1>Use Component</h1>
            <h2>{count}</h2>
            <button onClick={() => {
                    setCount(count+1)
            }}>Set</button>
        </div>
    )
}

const root = document.getElementById('app')
ReactDOM.render(<SomeComp />,root)
