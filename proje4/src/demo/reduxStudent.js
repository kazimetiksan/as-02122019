import {createStore} from 'redux'
import uuid from 'uuid'

const demoState = [{
    _id: uuid(),
    firstName: "Ateş",
    lastName: "Etiksan",
    classroom: "arılar"
},{
    _id: uuid(),
    firstName: "Kerem",
    lastName: "Demir",
    classroom: "arılar"
},{
    _id: uuid(),
    firstName: "Elif",
    lastName: "Tekin",
    classroom: "kelebekler"
}]

const studentReducer = (state=demoState,action) => {

    switch (action.type) {

        case 'ADD':
            return [...state, action.data]

        case 'REMOVE':
            return state.filter((item) => {
                return item._id != action._id
            })

        default: return state
    }
}

const store = createStore(studentReducer)

console.log(store.getState())

const addAction = ({
    firstName='',
    lastName='',
    classroom='arılar'
}) => {
    return {
        type: 'ADD',
        data: {
            _id: uuid(),
            firstName,
            lastName,
            classroom
        }
    }
}

store.dispatch(addAction({
    firstName: "Kazım",
    lastName: "Etiksan"
}))

console.log(store.getState())

const removeAction = (_id) => {
    return {
        type: 'REMOVE',
        _id
    }
}

const removingItem = store.getState()[0]
store.dispatch(removeAction(removingItem._id))

console.log(store.getState())