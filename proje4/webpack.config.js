const path = require('path')
const TerserPlugin = require('terser-webpack-plugin')
const UglifyJsPlugin = require('uglifyjs-webpack-plugin')

module.exports = (env='development') => {

  if (env != 'development' && env != 'production') {
    env = 'development'
  }

  console.log('webpack running mode: ',env)

  return {
    mode: env,
    entry: './src/app.js',
    output: {
      path: path.resolve(__dirname, 'public'),
      filename: 'script.js'
    },
    devtool: env == 'production' ? 'source-map' : 'cheap-module-eval-source-map',
    optimization: {
      minimize: env == 'production',
      minimizer: [new TerserPlugin()],
    },
    module: {
        rules: [{
            loader: 'babel-loader',
            test: /\.js$/,
            exclude: /node_modules/
        }]
    },
    devServer: {
      contentBase: path.resolve(__dirname, 'public'),
      historyApiFallback: true
    }
  }
}