class CounterApp extends React.Component {

    constructor (props) {
        super()

        this.state = {
            count: props.startsWith
        }

        this.onIncrease = this.onIncrease.bind(this)
        this.onDecrease = this.onDecrease.bind(this)
        this.onReset = this.onReset.bind(this)
    }

    onIncrease () {

        // // Function
        // metotIsmi (param1,param2) {
        //     return param1*param2
        // }

        // // Arrow Function
        // (param1,param2) => {
        //     return param1*param2
        // }

        this.setState((prevState) => {
            return {
                count: prevState.count+1
            }
        }, () => {
            console.log('state updated, current: ', this.state.count)
        })
    }

    onDecrease () {

        this.setState((prevState) => {
            return {
                count: prevState.count-1
            }
        })
    }

    onReset () {
        this.setState(() => {
            return {
                count: this.props.startsWith
            }
        })
    }

    render () {

        return (
            <div>
                <h1>Counter Application</h1>
                <h2>Current: {this.state.count}</h2>
                <button onClick={this.onDecrease}>Decrease</button>
                <button onClick={this.onReset}>Reset</button>
                <button onClick={this.onIncrease}>Increase</button>
            </div>
        )
    }
}


var root = document.getElementById('app')
ReactDOM.render(<CounterApp startsWith={0} />, root)