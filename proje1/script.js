'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var CounterApp = function (_React$Component) {
    _inherits(CounterApp, _React$Component);

    function CounterApp(props) {
        _classCallCheck(this, CounterApp);

        var _this = _possibleConstructorReturn(this, (CounterApp.__proto__ || Object.getPrototypeOf(CounterApp)).call(this));

        _this.state = {
            count: props.startsWith
        };

        _this.onIncrease = _this.onIncrease.bind(_this);
        _this.onDecrease = _this.onDecrease.bind(_this);
        _this.onReset = _this.onReset.bind(_this);
        return _this;
    }

    _createClass(CounterApp, [{
        key: 'onIncrease',
        value: function onIncrease() {
            var _this2 = this;

            // // Function
            // metotIsmi (param1,param2) {
            //     return param1*param2
            // }

            // // Arrow Function
            // (param1,param2) => {
            //     return param1*param2
            // }

            this.setState(function (prevState) {
                return {
                    count: prevState.count + 1
                };
            }, function () {
                console.log('state updated, current: ', _this2.state.count);
            });
        }
    }, {
        key: 'onDecrease',
        value: function onDecrease() {

            this.setState(function (prevState) {
                return {
                    count: prevState.count - 1
                };
            });
        }
    }, {
        key: 'onReset',
        value: function onReset() {
            var _this3 = this;

            this.setState(function () {
                return {
                    count: _this3.props.startsWith
                };
            });
        }
    }, {
        key: 'render',
        value: function render() {

            return React.createElement(
                'div',
                null,
                React.createElement(
                    'h1',
                    null,
                    'Counter Application'
                ),
                React.createElement(
                    'h2',
                    null,
                    'Current: ',
                    this.state.count
                ),
                React.createElement(
                    'button',
                    { onClick: this.onDecrease },
                    'Decrease'
                ),
                React.createElement(
                    'button',
                    { onClick: this.onReset },
                    'Reset'
                ),
                React.createElement(
                    'button',
                    { onClick: this.onIncrease },
                    'Increase'
                )
            );
        }
    }]);

    return CounterApp;
}(React.Component);

var root = document.getElementById('app');
ReactDOM.render(React.createElement(CounterApp, { startsWith: 0 }), root);
