import React from 'react'
import ReactDOM from 'react-dom'
import Dashboard from './components/Dashboard'
import AddNew from './components/AddNew'
import Header from './components/Header'
import {Route, BrowserRouter} from 'react-router-dom'


class App extends React.Component {

    render () {
        return (
            <BrowserRouter>
                <Header />
                <Route path="/" component={Dashboard} exact={true} />
                <Route path="/add" component={AddNew} />
            </BrowserRouter>
        )
    }
}

var root = document.getElementById('app')
ReactDOM.render(<App />, root)