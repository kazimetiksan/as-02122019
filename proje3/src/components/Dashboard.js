import React from 'react'
import StudentList from './StudentList'

export default class Dashboard extends React.Component {

    render () {

        return (
            <div>
                <h1>Dashboard</h1>
                <StudentList />
            </div>
        )
    }
}
