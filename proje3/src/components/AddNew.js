import React from 'react'
import NewForm from './NewForm'

export default class AddNew extends React.Component {

    render () {

        return (
            <div>
                <h1>Yeni Ekle</h1>
                <NewForm />
            </div>
        )
    }
}