import React from 'react'
import StudentRow from './StudentRow'
import NewForm from './NewForm'
import uuid from 'uuid'

export default class StudentList extends React.Component {

    constructor () {
        super()

        this.state = {
            students: [{
                _id: uuid(),
                firstName: "Ateş",
                lastName: "Etiksan",
                classroom: "arılar"
            },{
                _id: uuid(),
                firstName: "Kerem",
                lastName: "Demir",
                classroom: "arılar"
            },{
                _id: uuid(),
                firstName: "Elif",
                lastName: "Tekin",
                classroom: "kelebekler"
            }],
            editingStudent: undefined
        }

        this.addStudent = this.addStudent.bind(this)
        this.editStudent = this.editStudent.bind(this)
        this.updateStudent = this.updateStudent.bind(this)
        this.removeStudent = this.removeStudent.bind(this)
    }

    editStudent (editingId) {
        const editingStudent = this.state.students.find((item) => {
            return item._id == editingId
        })

        this.setState(() => {
            return {
                editingStudent
            }
        })
    }

    addStudent (newStudent) {
        console.log(newStudent)

        this.setState((prevState) => {
            return {
                students: [...prevState.students, newStudent]
            }
        })
    }

    removeStudent (removingId) {

        this.setState((prevState) => {

            const filteredStudents = prevState.students.filter((item) => {
                return item._id != removingId
            })

            return {
                students: filteredStudents
            }
        })
    }

    updateStudent (editingId, data) {

        this.setState((prevState) => {
            const updatedStudents = prevState.students.map((item) => {
                if (item._id == editingId) {
                    return {
                        ...item,
                        ...data
                    }
                }
                return item
            })

            return {
                students: updatedStudents,
                editingStudent: undefined
            }
        })
    }

    render () {

        const {students,editingStudent} = this.state

        return (
            <div>
                <table>
                    <tbody>
                        <tr>
                            <td>Ad</td>
                            <td>Soyad</td>
                            <td>Sınıf</td>
                        </tr>
                        {
                            students.map((std,index) => {
                                return (
                                    <StudentRow 
                                        key={index} 
                                        student={std}
                                        editStudent={this.editStudent}
                                        removeStudent={this.removeStudent}
                                    />
                                )
                            })
                        }
                    </tbody>
                </table>
                <NewForm 
                    addStudent={this.addStudent}
                    editingStudent={editingStudent}
                    updateStudent={this.updateStudent}
                />
            </div>
        )
    }
}

